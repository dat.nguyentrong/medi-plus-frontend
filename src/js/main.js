// Your main script
$(document).ready(function () {
  $('.testimonial-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }]
  });

  $('.clinic-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $("body").on("click", "#loginFormButton", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#registerForm').modal('hide');
    $('#loginForm').modal('show');
  });

  $("body").on("click", "#registerFormButton", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#loginForm').modal('hide');
    $('#registerForm').modal('show');
  });

  $('#booksForm').on('show.bs.modal', function (e) {
    $("#datepicker").datepicker("destroy");
    var form = $("#booksFormPopup").show();
    form.steps({
      onInit: function (event, current) {
        $('.actions > ul > li:first-child').attr('style', 'display:none');
        $('.actions > ul > li:nth-child(2) a').text('Chọn ngày');
        $("#datepicker").datepicker({
          minDate: new Date(),
        });
      },
      onStepChanged: function (event, current, next) {
        $('.actions > ul > li:first-child').attr('style', 'display:none');
        if (current === 0) {
          $('.actions > ul > li:nth-child(2) a').text('Chọn ngày');
        } else if (current === 1) {
          $('.actions > ul > li:nth-child(2) a').text('Chọn giờ');
        }
      },
      labels: {
        finish: 'Đặt lịch',
        next: 'Chọn ',
        previous: 'Quay lại'
      },
      headerTag: "h6",
      bodyTag: "section",
      transitionEffect: "fade",
      titleTemplate: '<span class="step-number-wrapper"><span class="step-number-block"><span class="number">#index#</span></span></span> <span class="step-text-wrapper">#title#</span>',
      onFinished: function (event, currentIndex) {
        $('#booksForm').modal('hide');
        $('#booksFormConfirm').modal('show');
      },
    });
  });

  $("body").on("click", "#thanksFormButton", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#booksFormConfirm').modal('hide');
    $('#booksFormThanks').modal('show');
  });

  $('#booksForm').on('hide.bs.modal', function (e) {
    $("#booksFormPopup").steps('destroy');
  });

  var header = $("#header");
  var scroll = $(window).scrollTop();
  if (scroll >= 145) {
    $('body').removeClass('clearHeader').addClass("darkHeader");
  } else {
    $('body').removeClass("darkHeader").addClass('clearHeader');
  }

  $(function () {
    //caches a jQuery object containing the header element
    $(window).scroll(function () {
      var scroll = $(window).scrollTop();

      if (scroll >= 145) {
        $('body').removeClass('clearHeader').addClass("darkHeader");
      } else {
        $('body').removeClass("darkHeader").addClass('clearHeader');
      }
    });
  });
});